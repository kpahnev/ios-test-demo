//
//  DetailsViewController.swift
//  TestDemo
//
//  Created by Kirill Pahnev on 22/01/2019.
//  Copyright © 2019 Kirill Pahnev. All rights reserved.
//

import UIKit

struct Movie: Codable {
    let title: String
    let overview: String
}

class DetailsViewController: UIViewController {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var overviewLabel: UILabel!

    var movie: Movie? {
        didSet {
            titleLabel.text = movie?.title
            overviewLabel.text = movie?.overview
        }
    }
    var movieId: Int?

    override func viewDidLoad() {
        super.viewDidLoad()
        titleLabel.accessibilityIdentifier = "Title"
        overviewLabel.accessibilityIdentifier = "Overview"
        
        guard let movieId = movieId else { return }

        URLSession.shared.dataTask(with: URL(string: "https://api.themoviedb.org/3/movie/\(movieId)?api_key=e8d272ad21f324980fa9c7f876b1fe1b&language=en-US&page=1")!) { (data, response, error) in
            print(error)
            guard let data = data else { return print("NO DATA") }
            guard let res = try? JSONDecoder().decode(Movie.self, from: data) else {
                return print("FAILED DECODING")
            }
            DispatchQueue.main.async {
                self.movie = res
            }
            }.resume()

    }
}
