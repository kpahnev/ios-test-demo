//
//  ViewController.swift
//  TestDemo
//
//  Created by Kirill Pahnev on 22/01/2019.
//  Copyright © 2019 Kirill Pahnev. All rights reserved.
//

import UIKit

struct MovieFragment: Codable {
    let title: String
    let id: Int
}

struct MoviesReponse: Codable {
    let results: [MovieFragment]
}

class ViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    var results = [MovieFragment]() {
        didSet {
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = self
        tableView.delegate = self
        tableView.accessibilityIdentifier = "MovieList"

        URLSession.shared.dataTask(with: URL(string: "https://api.themoviedb.org/3/movie/popular?api_key=e8d272ad21f324980fa9c7f876b1fe1b&language=en-US&page=1")!) { (data, response, error) in
            print(error)
            guard let data = data else { return print("NO DATA") }
            guard let res = try? JSONDecoder().decode(MoviesReponse.self, from: data) else {
                return print("FAILED DECODING")
            }
            self.results = res.results
        }.resume()
    }
}

extension ViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return results.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)

        cell.textLabel?.text = results[indexPath.row].title
        return cell
    }
}

extension ViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let movie = results[indexPath.row]
        let vc = storyboard?.instantiateViewController(withIdentifier: "DetailsViewController") as! DetailsViewController
        vc.movieId = movie.id
        navigationController?.pushViewController(vc, animated: true)
    }
}
