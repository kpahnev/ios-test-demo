appcenter test run xcuitest \
  --app "kirill.pahnev-cgi.com/UI-test-demo" \
  --devices "kirill.pahnev-cgi.com/ui_test_demo" \
  --test-series "master" \
  --locale "en_US" \
  --build-dir "../DerivedData/Build/Products/Debug-iphoneos" \
  --token $APPCENTER_LOGIN_TOKEN \
  --async