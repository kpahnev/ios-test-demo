//
//  TestDemoUITests.swift
//  TestDemoUITests
//
//  Created by Kirill Pahnev on 22/01/2019.
//  Copyright © 2019 Kirill Pahnev. All rights reserved.
//

import XCTest

class TestDemoUITests: XCTestCase {

    override func setUp() {
        let app = XCUIApplication()
        setupSnapshot(app)
        app.launch()

        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false

        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        XCUIApplication().launch()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func waitForElementToAppear(_ element: XCUIElement) -> Bool {
        let expectation = XCTKVOExpectation(keyPath: "exists", object: element,
                                            expectedValue: true)

        let result = XCTWaiter().wait(for: [expectation], timeout: 5)
        return result == .completed
    }

    func testExample() {
        XCTContext.runActivity(named: "Tap First Cell") { (activity) in
            let app = XCUIApplication()
            let tablesQuery = app.tables
            let cell = tablesQuery["MovieList"].cells.allElementsBoundByIndex.first
            waitForElementToAppear(cell!)
            snapshot("List")
            cell?.tap()
            snapshot("Details")
            let attachment = XCTAttachment(screenshot: XCUIScreen.main.screenshot())
            attachment.lifetime = .keepAlways
            activity.add(attachment)

            XCTAssert(app.staticTexts["Title"].exists)
            XCTAssert(app.staticTexts["Overview"].exists)
        }
    }

}
