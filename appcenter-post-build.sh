#!/usr/bin/env bash -e
LOGIN_TOKEN=$1

rm -rf "../DerivedData"
xcrun xcodebuild build-for-testing -derivedDataPath DerivedData -scheme TestDemo -allowProvisioningUpdates

appcenter test run xcuitest \
  --app "kirill.pahnev-cgi.com/iOS-demo" \
  --devices "DEVICE_SET_ID" \
  --test-series "master" \
  --locale "en_US" \
  --build-dir "DerivedData/Build/Products/Debug-iphoneos"
  --token $LOGIN_TOKEN